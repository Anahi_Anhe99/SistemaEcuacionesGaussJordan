﻿using System;

namespace GaussJordan
{
    public class Matriz
    {
        public int Filas { get; set; }
        public int Columnas { get; set; }
        public double[,] Elementos { get; set; }
    }
}
