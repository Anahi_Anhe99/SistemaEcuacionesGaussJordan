﻿using GaussJordan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SistemaEcuaciones
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MetodoGaussJordan ResolverSistema;
        public MainWindow()
        {
            InitializeComponent();
            ResolverSistema = new MetodoGaussJordan();
            Skpmatriz.IsEnabled = false;
            if (MessageBox.Show("¿Rellenar matriz usando archivos?", "Sistema de ecuaciones", MessageBoxButton.YesNoCancel, MessageBoxImage.Information) == MessageBoxResult.Yes)
            {
                RellenarMatriz();
            }
        }

        private void RellenarMatriz()
        {
        }

        private void BtnGenerarCampos_Click(object sender, RoutedEventArgs e)
        {
            Skpmatriz.IsEnabled = false;
            skpMatriz.Children.Clear();
            Matriz matriz = new Matriz()
            {
                Filas = int.Parse(txbFilas.Text),
                Columnas = int.Parse(txbColumnas.Text),
            };
            if (matriz.Filas <= 0 || matriz.Columnas <= 0)
            {
                MessageBox.Show("No puedes ingresar Filas y/o columnas menores o iguales a 0, rectifica tus datos", "Sistema de Ecuaciones", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                for (int i = 1; i <= matriz.Columnas; i++)
                {
                    StackPanel Fila = new StackPanel();

                    for (int j = 1; j <= matriz.Filas; j++)
                    {
                        TextBox Elemento = new TextBox();
                        Elemento.Width = 41;
                        Elemento.Height = 23;
                        Elemento.Name = $"elemento{i}_{j}";
                        Elemento.Margin = new Thickness(5, 5, 5, 5);
                        Fila.Children.Add(Elemento);
                    }
                    skpMatriz.Children.Add(Fila);
                }
                Skpmatriz.IsEnabled = true;
            }
        }

        private void BtnResolverSistema_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
